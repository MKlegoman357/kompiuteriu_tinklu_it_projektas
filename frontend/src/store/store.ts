import Vue from "vue";
import Vuex from "vuex";
import AuthModule, {AuthModuleState} from "@/store/AuthModule";
import VuexDecoratorsPlugin from "@/plugins/vuex-decorators";

export interface StoreState {
    auth: AuthModuleState;
}

Vue.use(Vuex);

export const authModule = new AuthModule();

export const store = new Vuex.Store<StoreState>({
    modules: {
        auth: authModule.getModule()
    },
    plugins: [VuexDecoratorsPlugin]
});
