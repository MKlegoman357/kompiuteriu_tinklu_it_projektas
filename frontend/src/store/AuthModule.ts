import {Action, Getter, Mutation, State, VuexModule} from "@/plugins/vuex-decorators";
import auth, {User} from "@/js/backend/auth";
import {StoreState} from "@/store/store";

export type AuthModuleState = {
    user: User | null;
    isLoggedIn: boolean;
};

export const tokenKey = "token";

export default class AuthModule extends VuexModule<AuthModuleState, StoreState> {

    @State
    user: User | null = null;

    @State
    token: string | null = null;

    initialized: boolean = false;
    initializing: boolean = false;
    initializeCallbacks: (() => void)[] = [];

    @Getter
    get isLoggedIn(): boolean {
        return this.user != null;
    }

    @Getter
    get isAdmin(): boolean {
        return this.isLoggedIn && (this.user as User).role === "admin";
    }

    @Getter
    get isUser(): boolean {
        return this.isLoggedIn && (this.user as User).role === "user";
    }

    constructor() {
        super();
        this.initVuexModule();
    }

    @Action
    init(): Promise<void> {
        return new Promise(resolve => {
            if (this.initialized) return resolve();
            if (this.initializing) return this.initializeCallbacks.push(resolve);

            let afterTokenUpdate = () => {
                this.initialized = true;
                this.initializing = false;

                for (let callback of this.initializeCallbacks) {
                    try {
                        callback();
                    } catch (error) {
                        console.log(error);
                    }
                }

                delete this.initializeCallbacks;

                resolve();
            };

            this.initializing = true;
            this.restoreToken();

            this.checkToken().then(() => afterTokenUpdate());
        });
    }

    @Action
    async register(email: string, password: string): Promise<boolean> {
        return await auth.register(email, password);
    }

    @Action
    async login(email: string, password: string): Promise<void> {
        const loginInfo = await auth.login(email, password);
        this.setLoginInfo(loginInfo.user, loginInfo.token);
    }

    @Action
    async logout() {
        if (this.user !== null || this.token !== null) {
            await auth.logout();
            this.setLoginInfo(null, null);
        }
    }

    @Action
    async checkToken() {
        if (this.token) {
            await auth.checkToken().then(loginInfo => {
                this.setLoginInfo(loginInfo.user, loginInfo.token);
            }, error => {
                console.log(error);
                this.setLoginInfo(null, null);
            });
        }
    }

    @Mutation
    setLoginInfo(user: User | null, token: string | null) {
        this.user = user;
        this.token = token;

        if (this.token) {
            localStorage.setItem(tokenKey, this.token);
        } else {
            localStorage.removeItem(tokenKey);
        }
    }

    @Mutation
    restoreToken() {
        this.token = localStorage.getItem(tokenKey);
    }

}
