export type ItemDialogState = "edit" | "create";

export interface ItemDialogTexts {
    createButton: string;

    create: {
        title: string;
        save: string;
        cancel: string;
        error: string;
    }

    edit: {
        title: string;
        save: string;
        cancel: string;
        error: string;
    }
}
