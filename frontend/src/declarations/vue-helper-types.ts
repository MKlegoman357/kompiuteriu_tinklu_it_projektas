export type ClassObject<T> = T extends new (...args: any) => infer R ? R : any;
