import {extend, localize, setInteractionMode} from "vee-validate";
import {confirmed, email, min, min_value, required} from "vee-validate/dist/rules";
import {i18n} from "@/plugins/i18n";

import en from "vee-validate/dist/locale/en.json";
import lt from "vee-validate/dist/locale/lt.json";

setInteractionMode("eager");

extend("required", required);
extend("confirmed", confirmed);
extend("email", email);
extend("min", min);
extend("min_value", min_value);

localize({en, lt});
localize(i18n.locale);
