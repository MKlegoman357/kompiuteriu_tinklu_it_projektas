import VueRouter, {Location, RouteConfig} from "vue-router";
import Vue from "vue";
import {authModule} from "@/store/store";

Vue.use(VueRouter);

export const defaultPage: Location = {name: "home"};
export const defaultUnauthorizedPage: Location = {name: "login"};
export const notFoundPage: Location = {name: "404"};

export const specialParams = {
    redirectTo: "redirectTo"
};

const routes: RouteConfig[] = [
    {
        name: "home",
        path: "/",
    },
    {
        name: "reservations",
        path: "/reservations",
        component: () => import("@/views/UserReservationsView.vue")
    },
    {
        name: "reserve-room",
        path: "/reserve-room",
        component: () => import("@/views/ReserveRoomView.vue")
    },
    {
        name: "admin",
        path: "/admin",
        meta: {layout: "no-container"},
        component: () => import("@/views/admin/MainView.vue"),
        redirect: {name: "admin/reservations"},
        children: [
            {
                name: "admin/reservations",
                path: "reservations",
                component: () => import("@/views/admin/ReservationsView.vue")
            },
            {
                name: "admin/rooms",
                path: "rooms",
                component: () => import("@/views/admin/RoomsView.vue")
            },
            {
                name: "admin/users",
                path: "users",
                component: () => import("@/views/admin/UsersView.vue")
            }
        ]
    },
    {
        name: "register",
        path: "/register",
        meta: {layout: "empty", noAuth: true},
        component: () => import("@/views/RegisterView.vue")
    },
    {
        name: "login",
        path: "/login",
        meta: {layout: "empty", noAuth: true},
        props: (route) => ({redirectTo: route.query[specialParams.redirectTo]}),
        component: () => import("@/views/LoginView.vue"),
        beforeEnter(to, from, next) {
            if (authModule.isLoggedIn) {
                next(defaultPage);
            }
            next();
        }
    }
];

export const router = new VueRouter({
    routes: routes,
    mode: "history"
});

router.beforeEach(async (to, from, next) => {
    await authModule.init();

    if (!authModule.isLoggedIn && !to.meta.noAuth) {
        next({
            ...defaultUnauthorizedPage,
            query: defaultUnauthorizedPage.query ? {
                ...defaultUnauthorizedPage.query,
                [specialParams.redirectTo]: to.fullPath
            } : {
                [specialParams.redirectTo]: to.fullPath
            }
        });
        return;
    }

    next();
});
