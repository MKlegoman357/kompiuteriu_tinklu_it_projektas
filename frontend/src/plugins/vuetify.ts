import Vue from "vue";
import Vuetify, {colors} from "vuetify/lib";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";

import lt from "vuetify/src/locale/lt";
import en from "vuetify/src/locale/en";
import {compareFn} from "vuetify/src/util/helpers";
import {Ripple} from "vuetify/lib/directives";

Vue.use(Vuetify, {
    directives: {
        Ripple
    }
});

export const vuetify = new Vuetify({
    theme: {
        dark: false,
        themes: {
            light: {
                primary: colors.green.darken2,
                secondary: colors.lightGreen.darken1,
                accent: colors.cyan.base,
                error: colors.red.base,
                warning: colors.orange.base,
                info: colors.blue.base,
                success: colors.green.base
            },
            dark: {
                primary: colors.green.darken2,
                secondary: colors.lightGreen.darken1,
                accent: colors.cyan.base,
                error: colors.red.base,
                warning: colors.orange.base,
                info: colors.blue.base,
                success: colors.green.base
            },
        }
    },
    lang: {
        locales: {lt, en},
        current: "lt"
    }
});

export type DataTableOptions = {
    page: number;
    itemsPerPage: number;
    sortBy: string[];
    sortDesc: boolean[];
    groupBy: string[];
    groupDesc: boolean[];
    multiSort: boolean;
    mustSort: boolean;
};

export type DataFooterOptions = {
    itemsPerPageOptions: number[];
};

export type TableHeader = {
    text: string;
    value: string;
    align?: 'start' | 'center' | 'end';
    sortable?: boolean;
    filterable?: boolean;
    divider?: boolean;
    class?: string | string[];
    width?: string | number;
    filter?: (value: any, search: string | null, item: any) => boolean;
    sort?: compareFn;
};

export type CalendarEvent = {
    name: string;
    start: string;
    end?: string;
};
