import Vue from "vue";
import DefaultLayout from "@/layouts/DefaultLayout.vue";
import EmptyLayout from "@/layouts/EmptyLayout.vue";
import NoContainerLayout from "@/layouts/NoContainerLayout.vue";

Vue.component("default-layout", DefaultLayout);
Vue.component("no-container-layout", NoContainerLayout);
Vue.component("empty-layout", EmptyLayout);
