import "reflect-metadata";

import Vue from "vue";
import App from "@/views/App.vue";
import {authModule, store} from "@/store/store";
import {router} from "@/plugins/router";
import {vuetify} from "@/plugins/vuetify";
import {i18n} from "@/plugins/i18n";
import "@/plugins/validation";
import "@/layouts/index";

Vue.config.productionTip = false;

authModule.init().catch(error => console.error(error));

new Vue({
    router: router,
    store: store,
    vuetify: vuetify,
    i18n: i18n,
    render: h => h(App)
}).$mount("#app");
