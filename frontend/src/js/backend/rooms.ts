import {Room} from "@/js/backend/admin/rooms";
import {postRequest} from "@/js/backend/backend-utils";

export const endpointPrefix = "rooms/";

export const endpoints = {
    all: endpointPrefix + "all",
    reserve: endpointPrefix + "reserve",
};

export interface Reservation {
    id: number;
    userId: number;
    from: string;
    to: string;
}

export interface RoomWithReservations extends Room {
    reservations: Reservation[];
}

export default {
    async all(): Promise<RoomWithReservations[]> {
        return postRequest(endpoints.all);
    },

    async reserve(rooms: number[], from: string, to: string): Promise<number> {
        return postRequest(endpoints.reserve, {rooms, from, to});
    },
};
