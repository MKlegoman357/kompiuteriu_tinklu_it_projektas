import {endpointPrefix as adminEndpointPrefix} from "@/js/backend/admin";
import {Pagination, postRequest} from "@/js/backend/backend-utils";

export const endpointPrefix = adminEndpointPrefix + "room/";

export const endpoints = {
    list: endpointPrefix + "list",
    create: endpointPrefix + "create",
    update: endpointPrefix + "update",
    delete: endpointPrefix + "delete",
};

export interface Room {
    id: number;
    name: string;
    bedCount: number;
    price: number;
}

export default {
    async list(page: number, itemsPerPage: number, sortColumn?: string, sortOrder?: -1 | 0 | 1): Promise<Pagination<Room>> {
        return postRequest(endpoints.list, {page, itemsPerPage, sortColumn, sortOrder});
    },

    async create(room: Room): Promise<number> {
        return postRequest(endpoints.create, room);
    },

    async update(room: Room): Promise<number> {
        return postRequest(endpoints.update, room);
    },

    async delete(room: Room): Promise<boolean> {
        return postRequest(endpoints.delete, room);
    },
};
