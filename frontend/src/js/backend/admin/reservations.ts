import {endpointPrefix as adminEndpointPrefix} from "@/js/backend/admin";
import {Pagination, postRequest} from "@/js/backend/backend-utils";
import {Reservation} from "@/js/backend/rooms";
import {User} from "@/js/backend/auth";
import {Room} from "@/js/backend/admin/rooms";

export const endpointPrefix = adminEndpointPrefix + "reservation/";

export const endpoints = {
    list: endpointPrefix + "list",
    confirm: endpointPrefix + "confirm",
    cancel: endpointPrefix + "cancel",
    delete: endpointPrefix + "delete",
};

export interface ReservationWithUser extends Reservation {
    user: User;
}

export interface ReservationWithRooms extends Reservation {
    rooms: Room[];
}

export type ReservationWithUserAndRooms = ReservationWithUser & ReservationWithRooms;

export default {
    async list(page: number, itemsPerPage: number, sortColumn?: string, sortOrder?: -1 | 0 | 1): Promise<Pagination<ReservationWithUserAndRooms>> {
        return postRequest(endpoints.list, {page, itemsPerPage, sortColumn, sortOrder});
    },

    async confirm(id: number): Promise<boolean> {
        return postRequest(endpoints.confirm, {id});
    },

    async cancel(id: number): Promise<boolean> {
        return postRequest(endpoints.cancel, {id});
    },

    async delete(id: number): Promise<boolean> {
        return postRequest(endpoints.delete, {id});
    },
};
