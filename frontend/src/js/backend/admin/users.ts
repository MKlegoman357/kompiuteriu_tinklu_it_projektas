import {endpointPrefix as adminEndpointPrefix} from "@/js/backend/admin";
import {Pagination, postRequest} from "@/js/backend/backend-utils";
import {User} from "@/js/backend/auth";

export const endpointPrefix = adminEndpointPrefix + "user/";

export const endpoints = {
    list: endpointPrefix + "list",
};

export default {
    async list(page: number, itemsPerPage: number, sortColumn?: string, sortOrder?: -1 | 0 | 1): Promise<Pagination<User>> {
        return postRequest(endpoints.list, {page, itemsPerPage, sortColumn, sortOrder});
    },
};
