import config from "@/config";
import {authModule} from "@/store/store";
import Cookies from "js-cookie";

type JsonResponse<T = any> = {
    ok: true;
    data: T;
} | {
    ok: false;
    error: {
        code: string;
        info?: any;
    };
};

export interface Pagination<T> {
    data: T[];
    page: number;
    totalCount: number;
    itemPerPage: number;
    sortOrder: -1 | 0 | 1;
    sortColumn?: string;
}

export const authTokenHeader = "Auth-Token";

export type BackendRequestErrorCode = "network-error" | "json-parse-error" | string;

export class BackendRequestError extends Error {

    code: BackendRequestErrorCode;
    info?: any;

    constructor(code: BackendRequestErrorCode = "network-error", message?: string, info?: any) {
        super(message);
        this.code = code;
        this.info = info;
    }

    toString(): string {
        return JSON.stringify({code: this.code, info: this.info});
    }
}

export function isBackendRequestError(error: any): error is BackendRequestError {
    return error instanceof BackendRequestError;
}

export function getBackendUrl(path?: string): string {
    return config.backendBaseUrl + (path ? path : "");
}

function addQueryParameter(url: string, name: string, value: string): string {
    name = encodeURIComponent(name);
    value = encodeURIComponent(value);
    return url + (url.includes("?") ? "&" : "?") + `${name}=${value}`;
}

/**
 * @throws BackendRequestError
 */
export async function postRequest<T = any>(endpoint: string, data?: object, updateAuthOnError: boolean = true): Promise<T> {
    try {
        let url = getBackendUrl(endpoint);

        if (process.env.NODE_ENV !== "production" && Cookies.get("XDEBUG_SESSION") != null)
            url = addQueryParameter(url, "XDEBUG_SESSION_START", "1");

        const response = await fetch(url, {
            method: "POST",
            body: typeof data === "undefined" ? undefined : JSON.stringify(data),
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                [authTokenHeader]: authModule.token || ""
            }
        });

        if (!response.ok && response.type === "error") throw new BackendRequestError("network-error");

        let jsonData: JsonResponse<T> = await response.json().catch(e => {
            throw new BackendRequestError("json-parse-error");
        });

        if (!jsonData.ok) {
            if (updateAuthOnError && jsonData.error.code === "access-denied") {
                authModule.checkToken();
            }

            throw new BackendRequestError(jsonData.error.code);
        }

        return jsonData.data;
    } catch (error) {
        if (process.env.NODE_ENV !== "production") console.log("postRequest", error.toString(), error);
        throw error;
    }
}
