import {Pagination, postRequest} from "@/js/backend/backend-utils";
import {ReservationWithRooms} from "@/js/backend/admin/reservations";

export const endpointPrefix = "reservations/";

export const endpoints = {
    list: endpointPrefix + "list",
    cancel: endpointPrefix + "cancel",
};

export default {
    async list(page: number, itemsPerPage: number, sortColumn?: string, sortOrder?: -1 | 0 | 1): Promise<Pagination<ReservationWithRooms>> {
        return postRequest(endpoints.list, {page, itemsPerPage, sortColumn, sortOrder});
    },

    async cancel(id: number): Promise<boolean> {
        return postRequest(endpoints.cancel, {id});
    },
};
