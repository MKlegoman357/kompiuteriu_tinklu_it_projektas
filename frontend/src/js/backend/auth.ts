import {postRequest} from "@/js/backend/backend-utils";

export const endpointPrefix = "auth/";

export const endpoints = {
    register: endpointPrefix + "register",
    login: endpointPrefix + "login",
    logout: endpointPrefix + "logout",
    checkToken: endpointPrefix + "check-token",
};

export type Role = "user" | "admin";

export interface User {
    id: number;
    email: string;
    role: Role;
}

export interface LoginInfo {
    token: string;
    user: User;
}

export default {
    async register(email: string, password: string): Promise<boolean> {
        return postRequest(endpoints.register, {email, password});
    },

    async login(email: string, password: string): Promise<LoginInfo> {
        return postRequest(endpoints.login, {email, password});
    },

    async logout(): Promise<boolean> {
        return postRequest(endpoints.logout);
    },

    async checkToken(): Promise<LoginInfo> {
        return postRequest(endpoints.checkToken, undefined, false);
    },
};
