<?php

use Propel\Runtime\Map\TableMap;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Routing\RouteCollectorProxy;
use sodyba\JsonResponse;
use sodyba\Pagination;
use sodyba\persistance\Reservation;
use sodyba\persistance\ReservationQuery;
use sodyba\persistance\ReservationRoomQuery;
use sodyba\persistance\Session;
use sodyba\Validator;

return function (RouteCollectorProxy $group) {
    $group->post("/list", function (Request $request, Response $response): Response {
        $pager = getPager($request, $response, ["id", "from", "to", "confirmed"], "from", 1);

        if (is_null($pager)) return $response;

        /** @var Session $session */
        $session = $request->getAttribute("session");

        $reservationsQuery = ReservationQuery::create()->filterByUser($session->getUser());
        [$reservations, $reservationsResult] = getPageData($reservationsQuery, $pager, Reservation::class);

        $reservationsData = [];
        foreach ($reservationsResult as $reservation) {
            $reservationData = $reservation->toArray(TableMap::TYPE_CAMELNAME);

            $reservationData["from"] = date_create($reservationData["from"])->format("Y-m-d");
            $reservationData["to"] = date_create($reservationData["to"])->format("Y-m-d");

            $reservationData["rooms"] = [];
            foreach ($reservation->getRooms() as $room) {
                $reservationData["rooms"][] = $room->toArray(TableMap::TYPE_CAMELNAME);
            }

            $reservationsData[] = $reservationData;
        }

        return withJson($response, JsonResponse::success(Pagination::create($reservationsData, $reservationsResult, $pager)));
    });

    $group->post("/cancel", function (Request $request, Response $response): Response {
        $validator = new Validator(["body" => ["object" => [
            "id" => ["int"]
        ]]], ["required"]);

        $values = validateBody($validator, $request, $response);

        if (is_null($values)) return $response;

        /** @var Session $session */
        $session = $request->getAttribute("session");

        $reservation = ReservationQuery::create()->filterByUser($session->getUser())->findOneById($values["id"]);

        if (is_null($reservation)) return withJson($response, JsonResponse::error("reservation-not-found"));
        if ($reservation->isConfirmed()) return withJson($response, JsonResponse::error("reservation-confirmed"));

        ReservationRoomQuery::create()->filterByReservationId($reservation->getId())->delete();
        $reservation->delete();

        return withJson($response, JsonResponse::success(true));
    });
};