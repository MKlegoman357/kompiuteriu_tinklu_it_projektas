<?php

use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Factory\AppFactory;
use Slim\Middleware\ContentLengthMiddleware;
use Slim\Routing\RouteContext;
use sodyba\JsonResponse;
use sodyba\Pager;
use sodyba\persistance\Session;
use sodyba\persistance\SessionQuery;
use sodyba\persistance\User;
use sodyba\RequestParams;
use sodyba\Utils;
use sodyba\Validator;

require __DIR__ . "/../lib/vendor/autoload.php";

define("AUTH_TOKEN_HEADER", "Auth-Token");

$app = AppFactory::create();

$app->setBasePath("/api");

/* Helper functions */

function withJson(Response $response, $data): Response
{
    $json = json_encode($data);
    $response->getBody()->write($json);
    return $response->withHeader("Content-Type", "application/json");
}

function validateParams(Validator $validator, Response &$response, $errorCode = "invalid-request"): ?array
{
    [$ok, $values, $errors] = $validator->parse(new RequestParams($_GET, $_POST, $_FILES));

    if ($ok) {
        return $values;
    }

    $response = withJson($response, JsonResponse::error($errorCode, $errors));

    return null;
}

function validateBody(Validator $validator, Request $request, Response &$response, $errorCode = "invalid-request"): ?array
{
    [$ok, $values, $errors] = $validator->parse(new RequestParams([], [
        "body" => $request->getParsedBody()
    ], []));

    if ($ok) {
        return $values["body"];
    }

    $response = withJson($response, JsonResponse::error($errorCode, $errors));

    return null;
}

function getPager(Request $request, Response &$response, array $columns = [], $defaultColumn = null, $defaultSort = 1, $errorCode = "invalid-pager"): ?Pager
{
    $paginationValidator = new Validator(["body" => ["object" => [
        "page" => ["int", "min" => 1, "default" => 1],
        "itemsPerPage" => ["int", "min" => 1, "max" => 100, "default" => 10],
        "sortColumn" => ["values" => $columns],
        "sortOrder" => ["int", "values" => [-1, 0, 1], "default" => 1]
    ]]], ["post", "required" => false]);

    $params = validateBody($paginationValidator, $request, $response, $errorCode);

    if (!$params) return null;

    $hasColumn = !is_null($params["sortColumn"]);

    return new Pager(
        $params["page"],
        $params["itemsPerPage"],
        $hasColumn ? $params["sortColumn"] : $defaultColumn,
        $hasColumn ? $params["sortOrder"] : $defaultSort
    );
}

function getPageData(ModelCriteria $query, Pager $pager, string $modelClass)
{
    if ($pager->sortColumn)
        $query = $query->orderBy(TableMap::translateFieldnameForClass($modelClass, $pager->sortColumn, TableMap::TYPE_CAMELNAME, TableMap::TYPE_COLNAME), $pager->sortOrder > 0 ? Criteria::ASC : Criteria::DESC);

    $rows = $query->paginate($pager->page, $pager->itemsPerPage);

    $data = [];
    foreach ($rows as $row) {
        $data[] = $row->toArray(TableMap::TYPE_CAMELNAME);
    }

    return [$data, $rows];
}

function updateSession(Session $session)
{
    $expires = new DateTime();
    $expires->add(new DateInterval("PT30M"));
    $session->setDateExpires($expires);
    $session->save();
}

function createSession(User $user): Session
{
    do {
        $sessionId = Utils::randomString(255);
    } while (SessionQuery::create()->filterById($sessionId)->exists());

    $session = new Session();
    $session->setId($sessionId);
    $session->setUser($user);
    return $session;
}

/**
 * @param Request $request
 * @return Response
 * @throws Exception
 */
function accessDenied(Request $request): Response
{
    $accepts = $request->getHeader("Accept");

    foreach ($accepts as $accept) {
        if ($accept === "application/json" || $accept === "text/json")
            return withJson(new \Slim\Psr7\Response(), JsonResponse::error("access-denied"));
    }

    throw new Exception("Access denied");
}

/** Custom middleware **/

/**
 * @param Request $request
 * @param RequestHandler $handler
 * @return Response
 * @throws Exception
 */
function authMiddleware(Request $request, RequestHandler $handler): Response
{
    $token = $request->getHeader(AUTH_TOKEN_HEADER);

    if (empty($token)) return accessDenied($request);

    $sessionId = $token[0];
    $session = SessionQuery::create()->findOneById($sessionId);

    if (is_null($session)) return accessDenied($request);

    if ($session->getDateExpires() < new DateTime) {
        $session->delete();
        return accessDenied($request);
    }

    updateSession($session);

    $request = $request->withAttribute("session", $session);

    return $handler->handle($request);
}

/**
 * @param Request $request
 * @param RequestHandler $handler
 * @return Response
 * @throws Exception
 */
function adminRoleMiddleware(Request $request, RequestHandler $handler): Response
{
    /** @var Session $session */
    $session = $request->getAttribute("session");

    if ($session->getUser()->getRole() !== "admin") return accessDenied($request);

    return $handler->handle($request);
}

/** Middleware **/

$app->add(new ContentLengthMiddleware());

$app->addBodyParsingMiddleware();

// This middleware will append the response header Access-Control-Allow-Methods with all allowed methods
$app->add(function (Request $request, RequestHandler $handler): Response {
    $routeContext = RouteContext::fromRequest($request);
    $routingResults = $routeContext->getRoutingResults();
    $methods = $routingResults->getAllowedMethods();
    $requestHeaders = $request->getHeaderLine("Access-Control-Request-Headers");

    $response = $handler->handle($request);

    $response = $response->withHeader("Access-Control-Allow-Origin", "*");
    $response = $response->withHeader("Access-Control-Allow-Methods", implode(",", $methods));
    $response = $response->withHeader("Access-Control-Allow-Headers", $requestHeaders);

    return $response;
});

$app->addRoutingMiddleware();

$app->addErrorMiddleware(true, true, true);

/** Routes **/

$app->group("/auth", require("auth.php"));

$app->group("/admin", require("admin.php"))->add("adminRoleMiddleware")->add("authMiddleware");

$app->group("/rooms", require("rooms.php"))->add("authMiddleware");
$app->group("/reservations", require("reservations.php"))->add("authMiddleware");

$app->any("/ping", function (Request $request, Response $response) {
    $response->getBody()->write("pong");
    return $response;
});

$app->options("/{routes:.+}", function (Request $request, Response $response) {
    return $response;
});

/** Run app **/

try {
    $app->run();
} catch (Throwable $exception) {
    var_dump($exception);
}