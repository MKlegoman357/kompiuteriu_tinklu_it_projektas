<?php

use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Propel;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Routing\RouteCollectorProxy;
use sodyba\JsonResponse;
use sodyba\Pagination;
use sodyba\persistance\Map\ReservationTableMap;
use sodyba\persistance\Reservation;
use sodyba\persistance\Room;
use sodyba\persistance\RoomQuery;
use sodyba\persistance\Session;
use sodyba\Validator;

return function (RouteCollectorProxy $group) {
    $group->post("/list", function (Request $request, Response $response): Response {
        $pager = getPager($request, $response, ["id", "name", "bedCount", "price"], "id");

        if (is_null($pager)) return $response;

        [$rooms, $roomsResult] = getPageData(RoomQuery::create(), $pager, Room::class);

        return withJson($response, JsonResponse::success(Pagination::create($rooms, $roomsResult, $pager)));
    });

    $group->post("/all", function (Request $request, Response $response): Response {
        $rooms = RoomQuery::create()->orderBy("name")->find();

        $roomsData = [];
        foreach ($rooms as $room) {
            $roomData = $room->toArray(TableMap::TYPE_CAMELNAME);

            $roomData["reservations"] = [];

            foreach ($room->getReservations() as $reservation) {
                $reservation = $reservation->toArray(TableMap::TYPE_CAMELNAME);
                unset($reservation["userId"]);
                $reservation["from"] = date_create($reservation["from"])->format("Y-m-d");
                $reservation["to"] = date_create($reservation["to"])->format("Y-m-d");
                $roomData["reservations"][] = $reservation;
            }

            $roomsData[] = $roomData;
        }

        return withJson($response, JsonResponse::success($roomsData));
    });

    $group->post("/reserve", function (Request $request, Response $response): Response {
        $validator = new Validator(["body" => ["object" => [
            "rooms" => ["array" => ["int"]],
            "from" => ["pattern" => "/^\\d{4}-\\d{2}-\\d{2}$/"],
            "to" => ["pattern" => "/^\\d{4}-\\d{2}-\\d{2}$/"],
        ]]], ["required"]);

        $values = validateBody($validator, $request, $response);

        if (is_null($values)) return $response;

        $from = date_create($values["from"]);
        $to = date_create($values["to"]);

        if ($to < $from) return withJson($response, JsonResponse::error("invalid-date-range"));
        if (empty($values["rooms"])) return withJson($response, JsonResponse::error("no-rooms"));

        /** @var Session $session */
        $session = $request->getAttribute("session");

        $con = Propel::getWriteConnection(ReservationTableMap::DATABASE_NAME);
        $con->beginTransaction();

        try {
            $rooms = [];

            foreach ($values["rooms"] as $roomId) {
                $room = RoomQuery::create()->findOneById($roomId);

                if (is_null($room)) return withJson($response, JsonResponse::error("room-not-found", $roomId));

                $reservations = $room->getReservations();

                foreach ($reservations as $reservation) {
                    $reservationFrom = $reservation->getFrom();
                    $reservationTo = $reservation->getTo();

                    if ($from <= $reservationTo && $to >= $reservationFrom) {
                        return withJson($response, JsonResponse::error("date-range-overlap", ["roomId" => $room->getId()]));
                    }
                }

                $rooms[] = $room;
            }

            $reservation = new Reservation();
            $reservation->setUser($session->getUser());
            $reservation->setFrom($from);
            $reservation->setTo($to);

            foreach ($rooms as $room) {
                $reservation->addRoom($room);
            }

            $reservation->save();

            $con->commit();

            return withJson($response, JsonResponse::success($reservation->getId()));
        } catch (Throwable $exception) {
            $con->rollBack();
            return withJson($response, JsonResponse::error("database-error", $exception));
        }
    });
};