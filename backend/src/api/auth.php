<?php

use Propel\Runtime\Map\TableMap;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Routing\RouteCollectorProxy;
use sodyba\JsonResponse;
use sodyba\persistance\Session;
use sodyba\persistance\User;
use sodyba\persistance\UserQuery;
use sodyba\Utils;
use sodyba\Validator;

return function (RouteCollectorProxy $group) {
    $group->post("/register", function (Request $request, Response $response): Response {
        $validator = new Validator(["body" => ["object" => [
            "email" => ["email", "max" => 255],
            "password" => ["max" => 255]
        ]]], ["required"]);

        $values = validateBody($validator, $request, $response);

        if (is_null($values)) return $response;

        if (UserQuery::create()->filterByEmail($values["email"])->exists())
            return withJson($response, JsonResponse::error("email-taken"));

        $user = new User();
        $user->setEmail($values["email"]);
        $user->setPassword(Utils::hash_password($values["password"]));
        $user->save();

        return withJson($response, JsonResponse::success(true));
    });

    $group->post("/login", function (Request $request, Response $response): Response {
        $validator = new Validator(["body" => ["object" => [
            "email" => ["email", "max" => 255],
            "password" => ["max" => 255]
        ]]], ["required"]);

        $values = validateBody($validator, $request, $response);

        if (is_null($values)) return $response;

        $user = UserQuery::create()->findOneByEmail($values["email"]);

        if (is_null($user))
            return withJson($response, JsonResponse::error("invalid-credentials"));

        $passwordValid = Utils::check_password($values["password"], $user->getPassword());

        if (!$passwordValid)
            return withJson($response, JsonResponse::error("invalid-credentials"));

        $session = createSession($user);
        updateSession($session);

        $userData = $user->toArray(TableMap::TYPE_CAMELNAME);
        unset($userData["password"]);

        return withJson($response, JsonResponse::success([
            "token" => $session->getId(),
            "user" => $userData
        ]));
    });

    $group->group("", function (RouteCollectorProxy $group) {
        $group->post("/logout", function (Request $request, Response $response): Response {
            /** @var Session $session */
            $session = $request->getAttribute("session");
            $session->delete();
            return withJson($response, JsonResponse::success(true));
        });

        $group->post("/check-token", function (Request $request, Response $response): Response {
            /** @var Session $session */
            $session = $request->getAttribute("session");

            $userData = $session->getUser()->toArray(TableMap::TYPE_CAMELNAME);
            unset($userData["password"]);

            return withJson($response, JsonResponse::success([
                "token" => $session->getId(),
                "user" => $userData
            ]));
        });
    })->add("authMiddleware");
};