<?php

use Propel\Runtime\Map\TableMap;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Routing\RouteCollectorProxy;
use sodyba\JsonResponse;
use sodyba\Pagination;
use sodyba\persistance\Reservation;
use sodyba\persistance\ReservationQuery;
use sodyba\persistance\ReservationRoomQuery;
use sodyba\persistance\Room;
use sodyba\persistance\RoomQuery;
use sodyba\persistance\User;
use sodyba\persistance\UserQuery;
use sodyba\Validator;

return function (RouteCollectorProxy $group) {
    $group->group("/room", function (RouteCollectorProxy $group) {
        $group->post("/create", function (Request $request, Response $response): Response {
            $validator = new Validator(["body" => ["object" => [
                "name" => ["max" => 255],
                "bedCount" => ["int", "min" => 0],
                "price" => ["int", "min" => 0]
            ]]], ["required"]);

            $values = validateBody($validator, $request, $response);

            if (is_null($values)) return $response;

            $nameTaken = RoomQuery::create()->filterByName($values["name"])->exists();

            if ($nameTaken) return withJson($response, JsonResponse::error("name-taken"));

            $room = new Room();
            $room->setName($values["name"]);
            $room->setBedCount($values["bedCount"]);
            $room->setPrice($values["price"]);
            $room->save();

            return withJson($response, JsonResponse::success($room->getId()));
        });

        $group->post("/list", function (Request $request, Response $response): Response {
            $pager = getPager($request, $response, ["id", "name", "bedCount", "price"], "id");

            if (is_null($pager)) return $response;

            [$rooms, $roomsResult] = getPageData(RoomQuery::create(), $pager, Room::class);

            return withJson($response, JsonResponse::success(Pagination::create($rooms, $roomsResult, $pager)));
        });

        $group->post("/update", function (Request $request, Response $response): Response {
            $validator = new Validator(["body" => ["object" => [
                "id" => ["int"],
                "name" => ["max" => 255],
                "bedCount" => ["int", "min" => 0],
                "price" => ["int", "min" => 0]
            ]]], ["required"]);

            $values = validateBody($validator, $request, $response);

            if (is_null($values)) return $response;

            $room = RoomQuery::create()->findOneById($values["id"]);

            if (is_null($room)) return withJson($response, JsonResponse::error("room-not-found"));

            $roomWithSameName = RoomQuery::create()->findOneByName($values["name"]);

            if (!is_null($roomWithSameName) && !$room->equals($roomWithSameName)) return withJson($response, JsonResponse::error("name-taken"));

            $room->setName($values["name"]);
            $room->setBedCount($values["bedCount"]);
            $room->setPrice($values["price"]);
            $room->save();

            return withJson($response, JsonResponse::success($room->getId()));
        });

        $group->post("/delete", function (Request $request, Response $response): Response {
            $validator = new Validator(["body" => ["object" => [
                "id" => ["int"]
            ]]], ["required"]);

            $values = validateBody($validator, $request, $response);

            if (is_null($values)) return $response;

            $room = RoomQuery::create()->findOneById($values["id"]);

            if (is_null($room)) return withJson($response, JsonResponse::error("room-not-found"));

            foreach ($room->getReservations() as $reservation) {
                $reservation->removeRoom($room);
                $reservation->save();

                if ($reservation->countRooms() === 0) {
                    $reservation->delete();
                }
            }

            $room->delete();

            return withJson($response, JsonResponse::success(true));
        });
    });

    $group->group("/reservation", function (RouteCollectorProxy $group) {
        $group->post("/list", function (Request $request, Response $response): Response {
            $pager = getPager($request, $response, ["id", "from", "to", "confirmed"], "from", 1);

            if (is_null($pager)) return $response;

            [$reservations, $reservationsResult] = getPageData(ReservationQuery::create(), $pager, Reservation::class);

            $reservationsData = [];
            foreach ($reservationsResult as $reservation) {
                $reservationData = $reservation->toArray(TableMap::TYPE_CAMELNAME);

                $reservationData["from"] = date_create($reservationData["from"])->format("Y-m-d");
                $reservationData["to"] = date_create($reservationData["to"])->format("Y-m-d");

                $reservationData["user"] = $reservation->getUser()->toArray(TableMap::TYPE_CAMELNAME);
                unset($reservationData["user"]["password"]);

                $reservationData["rooms"] = [];
                foreach ($reservation->getRooms() as $room) {
                    $reservationData["rooms"][] = $room->toArray(TableMap::TYPE_CAMELNAME);
                }

                $reservationsData[] = $reservationData;
            }

            return withJson($response, JsonResponse::success(Pagination::create($reservationsData, $reservationsResult, $pager)));
        });

        $group->post("/confirm", function (Request $request, Response $response): Response {
            $validator = new Validator(["body" => ["object" => [
                "id" => ["int"]
            ]]], ["required"]);

            $values = validateBody($validator, $request, $response);

            if (is_null($values)) return $response;

            $reservation = ReservationQuery::create()->findOneById($values["id"]);

            if (is_null($reservation)) return withJson($response, JsonResponse::error("reservation-not-found"));

            $reservation->setConfirmed(true);
            $reservation->save();

            return withJson($response, JsonResponse::success(true));
        });

        $group->post("/cancel", function (Request $request, Response $response): Response {
            $validator = new Validator(["body" => ["object" => [
                "id" => ["int"]
            ]]], ["required"]);

            $values = validateBody($validator, $request, $response);

            if (is_null($values)) return $response;

            $reservation = ReservationQuery::create()->findOneById($values["id"]);

            if (is_null($reservation)) return withJson($response, JsonResponse::error("reservation-not-found"));

            $reservation->setConfirmed(false);
            $reservation->save();

            return withJson($response, JsonResponse::success(true));
        });

        $group->post("/delete", function (Request $request, Response $response): Response {
            $validator = new Validator(["body" => ["object" => [
                "id" => ["int"]
            ]]], ["required"]);

            $values = validateBody($validator, $request, $response);

            if (is_null($values)) return $response;

            $reservation = ReservationQuery::create()->findOneById($values["id"]);

            if (is_null($reservation)) return withJson($response, JsonResponse::error("reservation-not-found"));

            ReservationRoomQuery::create()->filterByReservationId($reservation->getId())->delete();
            $reservation->delete();

            return withJson($response, JsonResponse::success(true));
        });
    });

    $group->group("/user", function (RouteCollectorProxy $group) {
        $group->post("/list", function (Request $request, Response $response): Response {
            $pager = getPager($request, $response, ["id", "email", "role"], "id");

            if (is_null($pager)) return $response;

            [$users, $usersResult] = getPageData(UserQuery::create(), $pager, User::class);

            $usersData = [];
            foreach ($users as $user) {
                unset($user["password"]);
                $usersData[] = $user;
            }

            return withJson($response, JsonResponse::success(Pagination::create($usersData, $usersResult, $pager)));
        });
    });
};