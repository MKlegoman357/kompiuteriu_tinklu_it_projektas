<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1576806185.
 * Generated on 2019-12-20 01:43:05 
 */
class PropelMigration_1576806185
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'sodyba' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE `reservation`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `from` DATE NOT NULL,
    `to` DATE NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `reservation_fi_29554a` (`user_id`),
    CONSTRAINT `reservation_fk_29554a`
        FOREIGN KEY (`user_id`)
        REFERENCES `user` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `reservation_room`
(
    `reservation_id` INTEGER NOT NULL,
    `room_id` INTEGER NOT NULL,
    PRIMARY KEY (`reservation_id`,`room_id`),
    INDEX `reservation_room_fi_0c6ff5` (`room_id`),
    CONSTRAINT `reservation_room_fk_0554bd`
        FOREIGN KEY (`reservation_id`)
        REFERENCES `reservation` (`id`),
    CONSTRAINT `reservation_room_fk_0c6ff5`
        FOREIGN KEY (`room_id`)
        REFERENCES `room` (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'sodyba' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `reservation`;

DROP TABLE IF EXISTS `reservation_room`;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}