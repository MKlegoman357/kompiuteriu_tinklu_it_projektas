<?php

namespace sodyba;

class Validator {
	const AS_ARRAY = "AS_ARRAY";
	const JSON_ARRAY = "JSON_ARRAY";

	/** @var array */
	static $rules = [];
	/** @var array */
	static $errorMessages = [];
	/** @var array */
	static $setups = [];

	/** @var array */
	private $params = [];
	/** @var array */
	private $messages = [];

	function __construct(array $params, array $config = []) {
		$config = $this->parseProps($config);

		$required = true;
		$method = "POST";
		$generateErrors = true;
		$default = null;

		foreach ($config as $key => $value) {
			if ($key === "required") {
				$required = $value !== false;
			} else if (in_array($key, ["post", "get", "files"])) {
				$method = strtoupper($key);
			} else if ($key === "errorMessages") {
				$this->messages = $value;
			} else if ($key === "errors") {
				$generateErrors = $value;
			} else if ($key === "default") {
				$default = $value;
			}
		}

		foreach ($params as $param => $rules) {
			$rules = $this->parseProps($rules);
			$paramData = [
				"required" => $required,
				"method" => $method,
				"label" => strval($param),
				"errors" => $generateErrors,
				"rules" => [],
				"errorMessages" => [],
				"default" => $default
			];

			foreach ($rules as $rule => $ruleParam) {
				if (in_array($rule, ["post", "get", "files"])) {
					$paramData["method"] = strtoupper($rule);
				} else if ($rule === "label") {
					$paramData["label"] = $ruleParam;
				} else if ($rule === "errors") {
					$paramData["errors"] = $ruleParam;
				} else if ($rule === "errorMessages") {
					$paramData["errorMessages"] = array_merge($paramData["errorMessages"], $ruleParam);
				} else if ($rule === "default") {
					$paramData["default"] = $ruleParam;
				} else if ($rule === "required") {
					$paramData["required"] = $ruleParam !== false;
				} else {
					$paramData["rules"][$rule] = $ruleParam;

					$setup = @static::$setups[$rule];

					if (Utils::isFunction($setup)) {
						$setup($paramData, $ruleParam, $rules, $required, $method, $generateErrors, $default, $this);
					}
				}
			}

			$this->params[$param] = $paramData;
		}
	}

	function parse(RequestParams $request) : array {
		$errors = [];
		$values = [];
		$hasErrors = false;

		$null = new class {};

		foreach ($this->params as $param => $data) {
			$value = $null;

			if ($data["method"] === "POST")
				$value = $request->getPost($param, $value);
			else if ($data["method"] === "GET")
				$value = $request->getGet($param, $value);
			else if ($data["method"] === "FILES") {
				$fileData = $request->getFile($param);

				if ($fileData) {
					if (is_array($fileData["error"])) {
						$value = [];

						foreach ($fileData["error"] as $i => $_) {
							$value[$i] = new UploadedFile(
								$fileData["name"][$i],
								$fileData["size"][$i],
								$fileData["tmp_name"][$i],
								$fileData["type"][$i],
								$fileData["error"][$i]
							);
						}
					} else {
						$value = new UploadedFile(
							$fileData["name"],
							$fileData["size"],
							$fileData["tmp_name"],
							$fileData["type"],
							$fileData["error"]
						);
					}
				}
			}

			$valueIsFile = self::isFile($value);

			if ($value === $null || $valueIsFile && !$value->isOk()) {
				if ($data["required"]) {
					$hasErrors = true;

					if ($valueIsFile && $value->status !== UPLOAD_ERR_NO_FILE) {
						$this->addErrorMessage($errors, $value, $data, "files");
					} else {
						$this->addErrorMessage($errors, $value, $data, "required");
					}
				}

				$values[$param] = $data["default"];

				continue;
			}

			foreach ($data["rules"] as $rule => $ruleParam) {
				@list($valid, $value, $errorMsg) = static::testRule($request, $value, $rule, $ruleParam, $data["label"], $this);

				if (!$valid) {
					$hasErrors = true;
					$this->addErrorMessage($errors, $value, $data, $rule, $ruleParam, $errorMsg);
					break;
				}
			}

			$values[$param] = $value;
		}

		return [!$hasErrors, $values, $errors];
	}

	private function addErrorMessage(array &$errors, $value, $paramData, $rule, $ruleParam = null, $errorMsg = null) : void {
		if ($paramData["errors"])
			$errors[] = static::getErrorMessage($value, $paramData, $rule, $ruleParam, $errorMsg, $this);
	}

	static function getErrorMessage($value, $paramData, $rule, $ruleParam, $errorMsg, Validator $validator) {
		$messageGenerator = null;

		if (!is_null($errorMsg))
			$messageGenerator = $errorMsg;
		else if (isset($paramData["errorMessages"][$rule]))
			$messageGenerator = $paramData["errorMessages"][$rule];
		else if (isset($validator->messages[$rule]))
			$messageGenerator = $validator->messages[$rule];
		else if (isset(static::$errorMessages[$rule]))
			$messageGenerator = static::$errorMessages[$rule];

		if (is_string($messageGenerator))
			return sprintf($messageGenerator, $value, $paramData["label"], $ruleParam);
		else if (Utils::isFunction($messageGenerator))
			return $messageGenerator($value, $paramData["label"], $ruleParam);

		return "Parameter ${paramData["label"]} does not meet requirement '$rule'.";
	}

	static function testRule(RequestParams $request, $value, $rule, $ruleParam, $label, Validator $validator) : array {
		$ruleFunc = null;

		if (Utils::isFunction($rule)) {
			$ruleFunc = $rule;
		} else if (isset(static::$rules[$rule])) {
			$ruleFunc = static::$rules[$rule];
		}

		if (is_null($ruleFunc)) return [true, $value];

		return $ruleFunc($request, $value, $ruleParam, $label, $validator);
	}

	static function parseProps($props) : array {
		$parsedProps = [];

		if (!is_array($props)) $props = [$props];

		foreach ($props as $key => $value) {
			if (is_numeric($key))
				$parsedProps[$value] = null;
			else
				$parsedProps[$key] = $value;
		}

		return $parsedProps;
	}

	static function isFile($value) {
		return $value instanceof UploadedFile;
	}
}

Validator::$errorMessages["required"] = '%2$s is required.';

Validator::$errorMessages["files"] = function (UploadedFile $value, $label) {
	$error = 'Unexpected error occurred while uploading $s';

	switch ($value->status) {
		case UPLOAD_ERR_INI_SIZE:
		case UPLOAD_ERR_FORM_SIZE:
			$error = '$s is too big.';
			break;
		case UPLOAD_ERR_PARTIAL:
			$error = '$s was only partially uploaded.';
	}

	return sprintf($error, $label);
};

Validator::$rules["min"] = function (RequestParams $request, $value, $amount) : array {
	if (!is_string($value) && is_numeric($value))
		return [$value >= $amount, $value];
	else if (is_array($value))
		return [count($value) >= $amount, $value];

	return [strlen($value) >= $amount, $value];
};
Validator::$errorMessages["min"] = function ($value, $label, $min) {
	if (!is_string($value) && is_numeric($value))
		return "$label cannot be less than $min.";
	else if (is_array($value))
		return "$label must have at least $min items.";
	else
		return "$label cannot be shorter than $min characters.";
};

Validator::$rules["max"] = function (RequestParams $request, $value, $amount) : array {
	if (!is_string($value) && is_numeric($value))
		return [$value <= $amount, $value];
	else if (is_array($value))
		return [count($value) <= $amount, $value];

	return [strlen($value) <= $amount, $value];
};
Validator::$errorMessages["max"] = function ($value, $label, $max) {
	if (!is_string($value) && is_numeric($value))
		return "$label cannot be greater than $max.";
	else if (is_array($value))
		return "$label cannot have more than $max items.";
	else
		return "$label cannot be longer than $max characters.";
};

Validator::$rules["int"] = function (RequestParams $request, $value) : array {
	if (is_numeric($value) && preg_match("#^-?\\d+$#", $value))
		return [true, intval($value)];

	return [false, $value];
};
Validator::$errorMessages["int"] = '%2$s must be an integer value.';

Validator::$rules["float"] = function (RequestParams $request, $value) : array {
	if (is_numeric($value))
		return [true, floatval($value)];

	return [false, $value];
};
Validator::$errorMessages["float"] = '%2$s must be a number value.';

Validator::$rules["bool"] = function (RequestParams $request, $value) : array {
	if (is_bool($value))
		return [true, $value];
	else if (in_array($val = strtolower(strval($value)), ["true", "false", "0", "1"]))
		return [true, $val === "true" || $val === "1"];

	return [false, $value];
};
Validator::$errorMessages["bool"] = '%2$s must be a boolean value.';

Validator::$rules["string"] = function (RequestParams $request, $value) : array {
	return [is_string($value), $value];
};
Validator::$errorMessages["string"] = '%2$s must be a string.';

Validator::$rules["pattern"] = function (RequestParams $request, $value, $pattern) : array {
	if (preg_match($pattern, $value))
		return [true, $value];

	return [false, $value];
};

Validator::$rules["email"] = function (RequestParams $request, $value) : array {
    if (preg_match("/^\\S+@\\S+\\.\\S+$/", $value))
        return [true, $value];

    return [false, $value];
};

Validator::$rules["values"] = function (RequestParams $request, $value, $values) : array {
	if (in_array($value, $values))
		return [true, $value];

	return [false, $value];
};
Validator::$errorMessages["values"] = function ($value, $label, $values) {
	return "$label can only be one of: " . implode(", ", $values) . "; $value given.";
};

Validator::$rules["json"] = function (RequestParams $request, $value, $rules, $label, Validator $validator) : array {
	$json = json_decode($value, isset($rules[Validator::JSON_ARRAY]));

	if (is_null($json))
		return [false, $value];

	foreach ($rules as $rule => $ruleParam) {
		if ($rule === Validator::JSON_ARRAY) continue;

		@list($valid, $json, $error) = Validator::testRule($request, $json, $rule, $ruleParam, $label, $validator);

		if (!$valid) {
			if (!isset($rules["label"]))
				$rules["label"] = $label;

			if ($rules["errors"])
				$error = $error ?? Validator::getErrorMessage($json, $rules, $rule, $ruleParam, null, $validator);
			else
				$error = null;

			return [false, $value, $error];
		}
	}

	return [true, $json];
};
Validator::$errorMessages["json"] = '%2$s must be a valid json value.';
Validator::$setups["json"] = function (&$paramData, $ruleParam, $rules, bool $required, string $method, bool $generateErrors, $default, Validator $validator) {
	$ruleParam = Validator::parseProps($ruleParam);

	foreach ($ruleParam as $rule => $param) {
		if (array_key_exists("required", $ruleParam))
			$ruleParam["required"] = $ruleParam["required"] !== false;
		else
			$ruleParam["required"] = $required;
		if (array_key_exists("errors", $ruleParam))
			$ruleParam["errors"] = $ruleParam["errors"] !== false;
		else
			$ruleParam["errors"] = $generateErrors;
		if (!array_key_exists("default", $ruleParam))
			$ruleParam["default"] = $default;

		$setup = @Validator::$setups[$rule];

		if (Utils::isFunction($setup)) {
			$setup($ruleParam, $param, $ruleParam, $required, $method, $generateErrors, $default, $validator);
		}

		if (isset($paramData["rules"]))
			$paramData["rules"]["json"] = $ruleParam;
		else
			$paramData["json"] = $ruleParam;
	}
};

Validator::$rules["array"] = function (RequestParams $request, $value, $data, $label, Validator $validator) : array {
	if (!is_array($value))
		return [false, $value];

	$updatedValues = [];

	foreach ($value as $key => $val) {
		$rules = Validator::parseProps($data);

		foreach ($rules as $rule => $ruleParam) {
			@list($valid, $val, $error) = Validator::testRule($request, $val, $rule, $ruleParam, "${label}[$key]", $validator);

			if (!$valid) {
				if (!isset($rules["label"]))
					$rules["label"] = "${label}[$key]";

				if ($rules["errors"])
					$error = $error ?? Validator::getErrorMessage($val, $rules, $rule, $ruleParam, null, $validator);
				else
					$error = null;

				return [false, $value, $error];
			}
		}

		$updatedValues[$key] = $val;
	}

	return [true, $updatedValues];
};
Validator::$errorMessages["array"] = '%2$s must be a valid array.';
Validator::$setups["array"] = function (&$paramData, $ruleParam, $rules, bool $required, string $method, bool $generateErrors, $default, Validator $validator) {
	$ruleParam = Validator::parseProps($ruleParam);

	foreach ($ruleParam as $rule => $param) {
		if (array_key_exists("required", $ruleParam))
			$ruleParam["required"] = $ruleParam["required"] !== false;
		else
			$ruleParam["required"] = $required;
		if (array_key_exists("errors", $ruleParam))
			$ruleParam["errors"] = $ruleParam["errors"] !== false;
		else
			$ruleParam["errors"] = $generateErrors;
		if (!array_key_exists("default", $ruleParam))
			$ruleParam["default"] = $default;

		$setup = @Validator::$setups[$rule];

		if (Utils::isFunction($setup)) {
			$setup($ruleParam, $param, $ruleParam, $required, $method, $generateErrors, $default, $validator);
		}

		if (isset($paramData["rules"]))
			$paramData["rules"]["array"] = $ruleParam;
		else
			$paramData["array"] = $ruleParam;
	}
};

Validator::$rules["sequential"] = function (RequestParams $request, $value) {
	return [is_array($value) && Utils::isSequential($value), $value];
};
Validator::$errorMessages["sequential"] = '%2$s must be a sequential array.';

Validator::$rules["object"] = function (RequestParams $request, $value, $data, $label, Validator $validator) : array {
	if (!is_array($value) && !is_object($value))
		return [false, $value];

	$props = is_array($value) ? $value : get_object_vars($value);

	$updatedValues = [];

	foreach ($data as $param => $rules) {
		$paramValue = @$props[$param];

		if (is_null($paramValue)) {
			if ($rules["required"]) {
				if (!isset($rules["label"]))
					$rules["label"] = "$label.$param";

				return [false, $value, $rules["errors"] ? Validator::getErrorMessage($paramValue, $rules, "required", true, null, $validator) : null];
			} else {
				$updatedValues[$param] = $rules["default"];
				continue;
			}
		}

		foreach ($rules as $rule => $ruleParam) {
			if ($rule === "required") continue;

			@list($valid, $paramValue, $error) = Validator::testRule($request, $paramValue, $rule, $ruleParam, "$label.$param", $validator);

			if ($valid) {
				$updatedValues[$param] = $paramValue;
			} else {
				if (!isset($rules["label"]))
					$rules["label"] = "$label.$param";

				if ($rules["errors"])
					$error = $error ?? Validator::getErrorMessage($paramValue, $rules, $rule, $ruleParam, null, $validator);
				else
					$error = null;

				return [false, $value, $error];
			}
		}
	}

	if (is_array($value)) {
		return [true, $updatedValues];
	}

	$value = new \stdClass();

	foreach ($updatedValues as $key => $val) {
		$value->$key = $val;
	}

	return [true, $value];
};
Validator::$errorMessages["object"] = '%2$s must be a valid object.';
Validator::$setups["object"] = function (&$paramData, $ruleParam, $rules, bool $required, string $method, bool $generateErrors, $default, Validator $validator) {
	foreach ($ruleParam as $name => $paramRules) {
		$paramRules = Validator::parseProps($paramRules);

		foreach ($paramRules as $rule => $param) {
			if (array_key_exists("required", $paramRules))
				$paramRules["required"] = $paramRules["required"] !== false;
			else
				$paramRules["required"] = $required;
			if (array_key_exists("errors", $paramRules))
				$paramRules["errors"] = $paramRules["errors"] !== false;
			else
				$paramRules["errors"] = $generateErrors;
			if (!array_key_exists("default", $paramRules))
				$paramRules["default"] = $default;

			$setup = @Validator::$setups[$rule];

			if (Utils::isFunction($setup)) {
				$setup($paramRules, $param, $paramRules, $required, $method, $generateErrors, $default, $validator);
			}

			if (isset($paramData["rules"]))
				$paramData["rules"]["object"][$name] = $paramRules;
			else
				$paramData["object"][$name] = $paramRules;
		}
	}
};

Validator::$rules["file"] = function (RequestParams $request, $value, $data) {
	if (!Validator::isFile($value))
		return [false, $value];

	if (!is_array($data))
		$data = [$data];

	if (!$data)
		return [true, $value];

	$valid = false;

	return [$valid, $value];
};
Validator::$errorMessages["file"] = function ($value, $label, $data) {
	if (!is_array($data)) $data = [$data];
	return "$label is not a valid " . implode(", ", $data) . " type file.";
};

Validator::$rules[Validator::AS_ARRAY] = function (RequestParams $request, $value) : array {
	if (is_object($value))
		return [true, get_object_vars($value)];

	return [is_array($value), $value];
};
Validator::$errorMessages[Validator::AS_ARRAY] = '%2$s must be an object or array.';

Validator::$rules["custom"] = function (RequestParams $request, $value, $data, $label, Validator $validator) : array {
	if (!is_array($data)) {
		$data = [$data];
	}

	$func = @$data[0];

	if (Utils::isFunction($func)) {
		return $func($request, $value, $label, $validator, ...array_slice($data, 1));
	}

	return [true, $value];
};

/*$validator = new Validator([
	"username" => ["required", "max" => 64, "label" => "Username"],
	"password" => ["required", "min" => 6, "max" => 64, "label" => "Password"],
	"accountType" => ["values" => ["user", "admin"], "default" => "user"],
	"id" => ["required", "int", "min" => 0, "errors" => false],
	"number" => ["float", "min" => -10, "max" => 10],
	"text" => ["pattern" => "#^[\w\s]+$#", "errorMessages" => [
		"pattern" => "Text can only consist of alphanumeric and whitespace characters."
	]],
	"simpleAjaxData" => ["json" => "bool"],
	"complexAjaxObjectArray" => ["json" => [
		"array" => [
			"object" => [
				"id" => ["int", "min" => 1],
				"name" => "string",
				"type" => ["string", "values" => ["type1", "type2", "type3"]],
				"data" => ["object", "required" => false]
			]
		]
	], "errors" => false]
], [
	"post",
	"errorMessages" => [
		"required" => '%2$s is required.',
		"min" => function ($value, $label, $min) {
			if (is_numeric($value))
				return "$label cannot be less than $min.";

			return "$label cannot be shorter than $min.";
		},
		"values" => function ($value, $label, $values) {
			return "$label can only be one of: " . implode(", ", $values) . "; $value given.";
		}
	]
]);*/