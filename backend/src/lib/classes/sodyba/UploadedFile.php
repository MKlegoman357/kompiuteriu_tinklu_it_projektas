<?php

namespace sodyba;

class UploadedFile extends \stdClass
{

    /** @var string */
    public $name;
    /** @var int */
    public $size;
    /** @var string */
    public $tmpName;
    /** @var string */
    public $mime;
    /** @var int */
    public $status;

    public function __construct(string $name, int $size, string $tmpName, string $mime, int $status)
    {
        $this->name = $name;
        $this->size = $size;
        $this->tmpName = $tmpName;
        $this->mime = $mime;
        $this->status = $status;
    }

    public function move(string $destination): bool
    {
        return @move_uploaded_file($this->tmpName, $destination);
    }

    public function isOk()
    {
        return $this->status === UPLOAD_ERR_OK;
    }

}