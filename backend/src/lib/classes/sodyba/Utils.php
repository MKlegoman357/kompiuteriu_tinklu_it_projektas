<?php

namespace sodyba;

use Closure;

abstract class Utils
{
    static function getRoot(string $path = ""): string
    {
        return dirname(__FILE__, 3) . "/" . $path;
    }

    static function normalizePath(string $path): string
    {
        $path = preg_replace("#[/\\\\]+#", "/", "/" . $path . "/");
        $path = preg_replace("#/\\./#", "/", $path);
        $path = preg_replace("#/\\.\\.+#", "/", $path);
        $path = preg_replace("#//+#", "/", $path);
        return trim($path, "/");
    }

    static function randomString(int $length = 32): string
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
        $str = "";

        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }

        return $str;
    }

    static function isFunction($var): bool
    {
        return is_object($var) && $var instanceof Closure;
    }

    static function isSequential(array $arr)
    {
        $count = count($arr);
        return $count === 0 || array_keys($arr) === range(0, $count - 1);
    }

    /* String utils */

    static function split(string $str, string $separator = " ", bool $removeEmpty = true): array
    {
        $values = explode($separator, $str);

        if ($removeEmpty) {
            return array_values(array_filter($values, function ($val) {
                return strlen($val) !== 0;
            }));
        }

        return $values;
    }

    static function startsWith(string $str, string $start): bool
    {
        return substr($str, 0, strlen($start)) === $start;
    }

    static function endsWith(string $str, string $end)
    {
        return substr($str, -strlen($end)) === $end;
    }

    /* Escaping functions */

    static function esc($obj): string
    {
        return htmlspecialchars(strval($obj), ENT_HTML5);
    }

    static function escattr($obj): string
    {
        return htmlspecialchars(strval($obj), ENT_QUOTES | ENT_HTML5);
    }

    static function escurl($obj): string
    {
        return urlencode(strval($obj));
    }

    /* Password functions */

    static function hash_password(string $password): string
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    static function check_password(string $password, string $hashedPassword): bool
    {
        return password_verify($password, $hashedPassword);
    }
}