<?php

namespace sodyba;

class Pager {

	public $page;
	public $pageIndex;
	public $itemsPerPage;
	public $sortColumn;
	public $sortOrder;

	public function __construct(int $pageIndex, int $itemsPerPage, ?string $sortColumn, int $sortOrder) {
		$this->pageIndex = $pageIndex;
		$this->page = $pageIndex;
		$this->itemsPerPage = $itemsPerPage;
		$this->sortColumn = $sortColumn;
		$this->sortOrder = $sortOrder;
	}

}