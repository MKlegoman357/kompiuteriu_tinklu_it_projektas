<?php

namespace sodyba;


class JsonResponse
{

    public static function success($data)
    {
        return [
            "ok" => true,
            "data" => $data
        ];
    }

    public static function error($code, $info = null)
    {
        $error = ["code" => $code];
        if (!is_null($info)) $error["info"] = $info;
        return [
            "ok" => false,
            "error" => $error
        ];
    }

}