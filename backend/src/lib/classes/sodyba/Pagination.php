<?php

namespace sodyba;

use Propel\Runtime\Util\PropelModelPager;

class Pagination {

	public static function create(array $data, PropelModelPager $propelPager, Pager $pager) : array {
		$pagination = [
			"data" => $data,
			"totalCount" => $propelPager->getNbResults(),
			"page" => $propelPager->getPage(),
			"itemsPerPage" => $propelPager->getMaxPerPage(),
			"sortOrder" => $pager->sortOrder
		];

		if ($pager->sortColumn) {
			$pagination["sortColumn"] = $pager->sortColumn;
		}

		return $pagination;
	}

}