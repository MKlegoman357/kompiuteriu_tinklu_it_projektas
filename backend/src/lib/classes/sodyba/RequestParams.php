<?php

namespace sodyba;

class RequestParams {

	private $get;
	private $post;
	private $files;

	public function __construct(array $get, array $post, array $files) {
		$this->get = $get;
		$this->post = $post;
		$this->files = $files;
	}

	public function getGet(string $key, $default = null) {
		return @$this->get[$key] ?? $default;
	}

	public function getPost(string $key, $default = null) {
		return @$this->post[$key] ?? $default;
	}

	public function getFile(string $key, $default = null) {
		return @$this->files[$key] ?? $default;
	}

}